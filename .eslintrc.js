module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
		jasmine: true
	},
	globals: {
		window: true,
		M: true
	},
	extends: ['plugin:vue/essential', 'eslint:recommended'],
	parserOptions: {
		parser: 'babel-eslint'
	},
	rules: {
		"no-mixed-spaces-and-tabs": [2, "smart-tabs"],
		'no-debugger': 0,
		'no-global-assign': 0
	}
}
