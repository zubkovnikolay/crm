module.exports = {
	devServer: {
		hot: true,
		port: 3454
	},
	publicPath: '/',
	assetsDir: 'assets',
	pwa: {
		appleMobileWebAppCapable: 'yes',
		appleMobileWebAppSatatusBarStyle: 'black',
		manifestOptions: {},
	}
}