CRM-приложение, написанное в рамках обучения Vue.js

# starosta

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test-watch
```

### Lints and fixes files
```
npm run lint
```
