import Vue from "vue"
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import VueMeta from 'vue-meta'
import App from "./App.vue"

import "./registerServiceWorker"
import router from "./router"
import store from "./store"

import capitalize from '@/filters/capitalize.filter'
import decapitalize from '@/filters/decapitalize.filter'
import currency from '@/filters/currency.filter'
import date from '@/filters/date.filter'
import localize from '@/filters/localize.filter'
import dateTitle from '@/filters/dateTitle.filter'

import tooltipDirective from '@/directives/tooltip.directive'

import messagePlugin from '@/utils/message.plugin'
import titlePlugin from '@/utils/title.plugin'
import Loader from '@/components/app/Loader'

import 'materialize-css/dist/css/materialize.min.css'
import 'material-design-icons/iconfont/material-icons.css'
import './assets/app.css'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.use(messagePlugin)
Vue.use(titlePlugin)
Vue.use(Vuelidate)
Vue.use(VueMeta)

Vue.component(Loader.name, Loader)
Vue.component('Paginate', Paginate)

Vue.filter('capitalize', capitalize)
Vue.filter('decapitalize', decapitalize)
Vue.filter('currency', currency)
Vue.filter('date', date)
Vue.filter('localize', localize)
Vue.filter('dateTitle', dateTitle)

Vue.directive('tooltip', tooltipDirective)

firebase.initializeApp({
	apiKey: "AIzaSyD4syvt5-hKjkk5F5mnTbNNKsQZVeqBZ5k",
	authDomain: "evklid-crm.firebaseapp.com",
	databaseURL: "https://evklid-crm.firebaseio.com",
	projectId: "evklid-crm",
	storageBucket: "evklid-crm.appspot.com",
	messagingSenderId: "153398151015",
	appId: "1:153398151015:web:c87a435e379aa7850651c2"
})

Vue.config.productionTip = false

let app

firebase.auth().onAuthStateChanged(() => !app ? app = new Vue({
	router,
	store,
	render: h => h(App)
}).$mount("#app") : ({}))