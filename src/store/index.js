import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import info from './info'
import category from './category'
import record from './record'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		error: null
	},
	mutations: {
		setError(state, error) {
			state.error = error
		},
		clearError(state) {
			state.error = null
		}
	},
	actions: {
		async fetchCurrency() {
			let res = await fetch(
				`https://api.exchangeratesapi.io/latest?symbols=USD,EUR,RUB&base=RUB`
			)
			res = await res.json()
			if (res.error) {
				throw new Error(res)
			}
			return res
		}
	},
	getters: {
		error: s => s.error
	},
	modules: {
		auth,
		info,
		category,
		record
	}
})
