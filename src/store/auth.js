import firebase from 'firebase/app'

export default {
	state: {
		user: null
	},
	mutations: {
		setUser(state, payload) {
			state.user = payload
		}
	},
	actions: {
		async login({ commit }, { email, password }) {
			// eslint-disable-next-line no-useless-catch
			try {
				await firebase.auth().signInWithEmailAndPassword(email, password)
				commit('setUser', firebase.auth().currentUser)
			} catch (e) {
				commit('setError', e)
				throw e
			}
		},
		async logout({ commit }) {
			await firebase.auth().signOut()
			await commit('clearInfo')
		},
		async register({ dispatch, commit }, { email, password, name } ) {
			try {
				await firebase.auth().createUserWithEmailAndPassword(email, password)
				const uid = await dispatch('getUid')
				await firebase.database().ref(`/users/${uid}/info`).set({
					name,
					account_balance: 10000
				})
			} catch (e) {
				commit('setError', e)
				throw e
			}

		},
		async getUid({ commit }) {
			const user = await firebase.auth().currentUser
			commit('setUser', user)
			return user ? user.uid : null
		}
	},
	getters: {
		user: s => s.user
	}
}