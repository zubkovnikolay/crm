export default {
	props: {
		rates: {
			type: Object,
			default: () => ({})
		},
	},
	data() {
		return {
			baseCurrency: 'RUB',
			currencies: ['RUB', 'USD', 'EUR']
		}
	}
}