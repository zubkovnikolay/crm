import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Account.vue'
import firebase from 'firebase/app'

Vue.use(VueRouter)

const router = new VueRouter({
			mode: 'history',
			base: process.env.BASE_URL,
			routes: [
				{
					path: '/login',
					name: 'Login',
					meta: { layout: 'empty' },
					component: () => import('../views/Login.vue')
				},
				{
					path: '/register',
					name: 'Register',
					meta: { layout: 'empty' },
					component: () => import('../views/Register.vue')
				},
				{
					path: '/',
					name: 'Home',
					meta: { auth: true },
					component: Home
				},
				{
					path: '/categories',
					name: 'Categories',
					meta: { auth: true },
					component: () => import('../views/Categories.vue')
				},
				{
					path: '/detail/:id',
					name: 'DetailRecord',
					props: true,
					meta: { auth: true },
					component: () => import('../views/Detail.vue')
				},
				{
					path: '/history',
					name: 'History',
					meta: { auth: true },
					component: () => import('../views/History.vue')
				},
				{
					path: '/planning',
					name: 'Planning',
					meta: { auth: true },
					component: () => import('../views/Planning.vue')
				},
				{
					path: '/profile',
					name: 'Profile',
					meta: { auth: true },
					component: () => import('../views/Profile.vue')
				},
				{
					path: '/record',
					name: 'Record',
					meta: { auth: true },
					component: () => import('../views/Record.vue')
				}]
		}
)

router.beforeEach((to, from, next) => {
	const currentUser = firebase.auth().currentUser
	const requiredAuth = to.matched.some(r => r.meta.auth)
	if (!currentUser && requiredAuth) {
		next('/login?message=login')
	} else {
		next()
	}
})

export default router
