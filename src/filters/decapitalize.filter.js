export default function decapitalize(value) {
	if (!value) return ''
	return value.toString().charAt(0).toLocaleLowerCase() + value.slice(1)
}