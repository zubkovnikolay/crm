import store from '../store'

export default function dateFilter(value, format = 'date') {
	const locale = store.getters.info?.locale || 'ru-RU'
	return new Intl.DateTimeFormat(locale, {
				day: '2-digit',
				month: 'long',
				year: 'numeric',
				...(format.includes('time')) && {
					hour: '2-digit',
					minute: '2-digit',
					second: '2-digit'
				}
			}
	).format(new Date(value))
}