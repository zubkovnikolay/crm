import localizeFilter from '@/filters/localize.filter'

export default function dateTitleFilter(value) {
	const date = new Date(value)
	const now = new Date()

	const diff = Math.floor(Math.abs(now - date) / 86400000)

	if (diff === 1) {
		return localizeFilter('Yesterday')
	} else if (diff < 1) {
		return localizeFilter('Today')
	}

	return value
}