import store from '../store'
import ru from '../locales/ru'
import en from '../locales/en'

const locales = {
	'ru-RU': ru,
	'en-US': en
}

export default function localizeFilter(key, ...args) {

	const locale = store.getters.info?.locale || 'ru-RU'
	if (!locales[locale][key]) {
		debugger
		return `[Localize error]: ${key} not found`
	}

	return typeof locales[locale][key] === 'function' ? locales[locale][key](args) : locales[locale][key]
}