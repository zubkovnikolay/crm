import M from 'materialize-css'
import localize from '@/filters/localize.filter'

export default {
	install(Vue) {
		Vue.prototype.$message = function(html, ...args) {
			M.toast({html: localize(html, args)})
		}

		Vue.prototype.$error = function(html, ...args) {
			M.toast({html: `[${localize('Error')}]: ${localize(html, args)}`})
		}
	}
}