export default {
	'logout': 'UserLoggedOut',
	'login': 'SignInToUse',
	'auth/user-not-found': 'UserNotFound',
	'auth/wrong-password': 'UserNotFound',
	'auth/email-already-in-use': 'EmailInUse',
}